// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "ShipPawn.h"
#include "ShipBooster.h"

// Sets default values
AShipPawn::AShipPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShipPawn::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = StartHealth;
	CurrentEnergy = StartEnergy;
}

// Called every frame
void AShipPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	//UE_LOG(LogTemp, Warning, TEXT("ShipState: %d"), (int)ShipState);

	//Update Score:

	Score += DeltaTime * ScoreMultiplier;

}

// Called to bind functionality to input
void AShipPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

void AShipPawn::Initialise(UShipBooster* BoosterToSet)
{
	Booster = BoosterToSet;
}

float AShipPawn::GetHealthPercent() const {
	return (float)CurrentHealth / (float)StartHealth;
}

float AShipPawn::GetEnergyPercent() const {
	return (float)CurrentEnergy / (float)StartEnergy;
}

int32 AShipPawn::GetScore() const {
	return (int32)Score;
}

int32 AShipPawn::GetScoreMultiplier() const {
	return ScoreMultiplier;
}

EShipState AShipPawn::GetShipState()
{
	return ShipState;
}

float AShipPawn::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent,
	class AController * EventInstigator, AActor * DamageCauser)
{
	//TODO Decide if ship is invincible during dodge
	if (ShipState == EShipState::Normal)
	{ 

		//UE_LOG(LogTemp, Warning, TEXT("Reached damage portion"))
		//UE_LOG(LogTemp, Warning, TEXT("Damage amount : %f"), DamageAmount )
		int32 DamagePoints = FPlatformMath::RoundToInt(DamageAmount);
		int32 DamageToApply = FMath::Clamp<int32>(DamagePoints, 0, CurrentHealth);

		CurrentHealth -= DamageToApply;

		//Don't make the player invincible if hit by pain causing volume
		if (!Cast<APainCausingVolume>(DamageCauser)) 
		{

			//Reset score Multiplier if hit
			ScoreMultiplier = 1;

			ShipState = EShipState::Invincible;

			FTimerHandle Timer;
			GetWorld()->GetTimerManager().SetTimer(
				Timer,
				this,
				&AShipPawn::ResetInvicibilityAndDodge,
				InvincibleDelay,
				false
			);
		}

		if (CurrentHealth <= 0)
		{
			OnShipDeath.Broadcast();
		}

		return DamageToApply;
	}

	return DamageAmount;
}

void AShipPawn::ResetInvicibilityAndDodge()
{
	ShipState = EShipState::Normal;
}

void AShipPawn::Heal(int32 amount)
{
	CurrentHealth = FMath::Clamp(CurrentHealth + amount, CurrentHealth, StartHealth);
}

void AShipPawn::Charge(int32 amount)
{
	CurrentEnergy = FMath::Clamp(CurrentEnergy + amount, CurrentEnergy, StartEnergy);
}

void AShipPawn::Accelerate()
{
	ScoreMultiplier++;
	Booster->Accelerate();
}

bool AShipPawn::IsDodging() {
	return ShipState == EShipState::Dodging;
}


bool AShipPawn::MakeShipDodge()
{

	if (CanShipDodge() && ShipState != EShipState::Dodging)
	{

		ShipState = EShipState::Dodging;
		CurrentEnergy -= DodgeEnergyCost;

		FTimerHandle Timer;
		GetWorld()->GetTimerManager().SetTimer(
			Timer,
			this,
			&AShipPawn::ResetInvicibilityAndDodge,
			DodgeDelay,
			false
		);

		return true;
	}

	return false;
}

bool AShipPawn::CanShipDodge() const
{
	return CurrentEnergy >= DodgeEnergyCost;
}