// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "ShipPlayerController.h"
#include "ShipPawn.h"
#include "Blueprint/UserWidget.h"

void AShipPlayerController::BeginPlay()
{
	Super::BeginPlay();

	AShipPawn* Ship = Cast<AShipPawn>(GetPawn());
	Ship->OnShipDeath.AddUniqueDynamic(this, &AShipPlayerController::OnPossessedShipDeath);

	if (WLoseScreen) {

		WMyLoseScreen = CreateWidget<UUserWidget>(this, WLoseScreen);

	}


}


void AShipPlayerController::OnPossessedShipDeath()
{
	StartSpectatingOnly();



	
	FTimerHandle Timer;
	GetWorld()->GetTimerManager().SetTimer(
		Timer,
		this,
		&AShipPlayerController::LeaveGame,
		0.5f,
		false
	);

}

void AShipPlayerController::LeaveGame() {

	if (WMyLoseScreen)
	{
		UGameplayStatics::SetGamePaused(GetWorld(), true);
		//let add it to the view port
		WMyLoseScreen->AddToViewport();
	}

	//UGameplayStatics::OpenLevel(GetWorld(), "MainMenu");
}