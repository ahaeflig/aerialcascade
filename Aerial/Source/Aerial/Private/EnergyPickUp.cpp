// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "EnergyPickUp.h"
#include "ShipPawn.h"


// Sets default values
AEnergyPickUp::AEnergyPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Collision Mesh"));
	StaticMesh->SetNotifyRigidBodyCollision(true);
	SetRootComponent(StaticMesh);

	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AEnergyPickUp::TriggerEnter);
	StaticMesh->OnComponentEndOverlap.AddDynamic(this, &AEnergyPickUp::TriggerExit);
}

// Called when the game starts or when spawned
void AEnergyPickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnergyPickUp::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AEnergyPickUp::TriggerEnter(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//TODO check if better way to check if Pawn Hit
	if (Cast<AShipPawn>(OtherActor)) {
		APlayerController* CP = UGameplayStatics::GetPlayerController(GetWorld(), 0);

		AShipPawn* Ship = Cast<AShipPawn>(CP->GetPawn());
		Ship->Charge(EnergyAmount);

		Destroy();
	}
}

void AEnergyPickUp::TriggerExit(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}