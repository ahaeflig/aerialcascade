// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "BasicObstacle.h"
#include "ShipPawn.h"


// Sets default values
ABasicObstacle::ABasicObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Collision Mesh"));
	SetRootComponent(CollisionMesh);
	CollisionMesh->SetNotifyRigidBodyCollision(true);
	CollisionMesh->SetVisibility(true);
	CollisionMesh->SetSimulatePhysics(true);

	CollisionMesh->OnComponentHit.AddDynamic(this, &ABasicObstacle::OnHit);

}

// Called when the game starts or when spawned
void ABasicObstacle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasicObstacle::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ABasicObstacle::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse, const FHitResult& Hit) {

	//UE_LOG(LogTemp, Warning, TEXT("Impact point loc %s"), *Hit.ImpactPoint.ToString())

	if (Cast<AShipPawn>(OtherActor)) {
		UGameplayStatics::ApplyRadialDamage(
			this,
			DamageAmountOnHit,
			Hit.ImpactPoint,
			DamageRadius,
			UDamageType::StaticClass(),
			TArray<AActor*>() //Damage all actors
		);
	}

}


