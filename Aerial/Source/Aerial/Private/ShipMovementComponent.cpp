// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "ShipMovementComponent.h"
#include "ShipBooster.h"


UShipMovementComponent::UShipMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UShipMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	MoveForward();
	//Lerp rotation to target rotation
	//TargetRotation.Normalize();

	auto RootComp = GetOwner()->GetRootComponent();
	FRotator CurrRot = RootComp->RelativeRotation;

	FRotator RandomTilt = FRotator(FMath::RandRange(-5.f, 5.f), 0.f, FMath::RandRange(-5.f, 5.f));
	TargetRotation.Add( RandomTilt.Pitch, RandomTilt.Yaw, 15.f * PhoneTilt.Y + RandomTilt.Roll);

	RootComp->SetRelativeRotation(
		FMath::Lerp(CurrRot, TargetRotation, 0.05),
		false
	);
}

void UShipMovementComponent::Initialise(UShipBooster* BoosterToSet)
{
	Booster = BoosterToSet;
}

// Called when the game starts or when spawned
void UShipMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	//Viewport Size 
	const FVector2D ViewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
	const FVector2D  ViewportCenter = FVector2D(ViewportSize.X / 2, ViewportSize.Y / 2);

	//Initialized at the center
	FirstFingerCurrent = ViewportCenter;
	FirstFingerLast = ViewportCenter;

	SecondFingerCurrent = ViewportCenter;
	SecondFingerLast = ViewportCenter;
}

void UShipMovementComponent::MoveForward()
{
	if (!ensure(Booster)) { return; }
	Booster->MoveForward();
}

void UShipMovementComponent::IntendMoveUp(float AxisValue)
{
}

void UShipMovementComponent::IntendMoveRight(float AxisValue)
{
}

void UShipMovementComponent::GetTouchInformation(FVector2D FirstFinger,
		bool isPressedFirstFinger, FVector2D SecondFinger, bool isPressedSecondFinger)
{
	FirstFingerCurrent = FirstFinger;
	isFirstFingerPressed = isPressedFirstFinger;

	SecondFingerCurrent = SecondFinger;
	isSecondFingerPressed = isPressedSecondFinger;
}

void UShipMovementComponent::GetPhoneTilt(FVector Vec)
{
	PhoneTilt = Vec;
}


void UShipMovementComponent::ManageMovement() {

	//Viewport Size
	const FVector2D ViewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
	const FVector2D  ViewportCenter = FVector2D(ViewportSize.X / 2, ViewportSize.Y / 2);

	//Reset Touch amount if not pressed for vector final movement addition

	//
	//

	FVector2D MoveFirstSwipe = FVector2D(0.f, 0.f);
	FVector2D MoveSecondSwipe = FVector2D(0.f, 0.f);

	if (isFirstFingerPressed) {
		MoveFirstSwipe = FirstFingerLast - FirstFingerCurrent;	
		MoveFirstSwipe = FVector2D(MoveFirstSwipe.X / ViewportSize.X, MoveFirstSwipe.Y / ViewportSize.Y);
	}
	else {
		FirstFingerCurrent = FVector2D(ViewportCenter.X, ViewportCenter.Y);
	}

	if (isSecondFingerPressed) {
		MoveSecondSwipe = SecondFingerLast - SecondFingerCurrent;
		MoveSecondSwipe = FVector2D(MoveSecondSwipe.X / ViewportSize.X, MoveSecondSwipe.Y / ViewportSize.Y);
	}
	else {
		SecondFingerCurrent = FVector2D(ViewportCenter.X, ViewportCenter.Y);
	}

	//Get Touch Difference with center for vector direction
	FVector2D MoveFirstFinger = ViewportCenter - FirstFingerCurrent;
	FVector2D MoveSecondFinger = ViewportCenter - SecondFingerCurrent;

	float XFinal = -(MoveFirstFinger.X + MoveSecondFinger.X);
	float YFinal = MoveFirstFinger.Y + MoveSecondFinger.Y;

	//Get percent of screen covered to check for swipe detection
	
	float FirstFingerRangePercent = MoveFirstSwipe.Size();
	float SecondFingerRangePercent = MoveSecondSwipe.Size();


	//if (isFirstFingerPressed || isSecondFingerPressed) {
		ManageShipTilt(XFinal, YFinal);
	//	}
	/*else {
		ManageShipTilt(0.f, 0.f);
	}*/

	//UE_LOG(LogTemp, Warning, TEXT("First finger range percent : %f"), FirstFingerRangePercent)

	if (FirstFingerRangePercent > SwipeMoveThresholdPercent)
	{
		Booster->DodgeFromTouch(-MoveFirstSwipe.X, MoveFirstSwipe.Y);
	}
	else if (SecondFingerRangePercent > SwipeMoveThresholdPercent) 
	{
		Booster->DodgeFromTouch(-MoveSecondSwipe.X, MoveSecondSwipe.Y);
	}
	else
	{
		Booster->MoveFromTouch(XFinal, YFinal);
	}


//	if (isFirstFingerPressed) {
		FirstFingerLast = FirstFingerCurrent;
//	}

//	if (isSecondFingerPressed) {
		SecondFingerLast = SecondFingerCurrent;
//	}


	/*
	if (isPressed) {

		float MoveX = ViewportCenter.X - X;
		float MoveY = ViewportCenter.Y - Y;

		
		ManageShipTilt(-MoveX, MoveY);

		if (MoveRangePercent > SwipeMoveThresholdPercent)
		{
			Booster->DodgeFromTouch(-MoveXSwipePercent, MoveYSwipePercent);
		}
		else
		{
			Booster->MoveFromTouch(-MoveX, MoveY);
		}

	}
	else {
		ManageShipTilt(0.f, 0.f);
	}

	LastX = X;
	LastY = Y;
	*/
}


void UShipMovementComponent::ManageShipTilt(float MoveX, float MoveY)
{
	FVector2D RotDir =  FVector2D(MoveX, MoveY);
	RotDir.Normalize();
	TargetRotation = FRotator(TiltAngleFactor*RotDir.Y, TiltAngleFactor*RotDir.X, 0.f);
}

void UShipMovementComponent::TouchInitFirstFinger(FVector2D Vec)
{
	FirstFingerLast = Vec;
}

void UShipMovementComponent::TouchInitSecondFinger(FVector2D Vec)
{
	SecondFingerLast = Vec;
}