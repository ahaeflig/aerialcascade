// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "BasicTurret.h"
#include "ShipPawn.h"
#include "BasicProjectile.h"


// Sets default values
ABasicTurret::ABasicTurret()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Collision Mesh"));
	StaticMesh->SetNotifyRigidBodyCollision(true);
	SetRootComponent(StaticMesh);

	StaticMesh->OnComponentHit.AddDynamic(this, &ABasicTurret::OnHit);
}

// Called when the game starts or when spawned
void ABasicTurret::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasicTurret::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABasicTurret::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse, const FHitResult& Hit) {

	if (Cast<AShipPawn>(OtherActor)) {
		UGameplayStatics::ApplyRadialDamage(
			this,
			DamageAmountOnHit,
			GetActorLocation(),
			DamageRadiusOnHit,
			UDamageType::StaticClass(),
			TArray<AActor*>() //Damage all actors
		);
	}

}

void ABasicTurret::Fire()
{
	if (!ensure(ProjectileBlueprint)) { return; }

	auto ProjectileLeft = GetWorld()->SpawnActor<ABasicProjectile>(
		ProjectileBlueprint,
		StaticMesh->GetSocketLocation(FName("ProjectileLeft")),
		StaticMesh->GetSocketRotation(FName("ProjectileLeft"))
	);

	auto ProjectileRight = GetWorld()->SpawnActor<ABasicProjectile>(
		ProjectileBlueprint,
		StaticMesh->GetSocketLocation(FName("ProjectileRight")),
		StaticMesh->GetSocketRotation(FName("ProjectileRight"))
	);

	if (ProjectileLeft) {
		ProjectileLeft->LaunchProjectile(LaunchSpeed);

	}

	ProjectileRight->LaunchProjectile(LaunchSpeed);
	
}