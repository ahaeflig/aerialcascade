// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "AccelerationPickUp.h"
#include "ShipPawn.h"


// Sets default values
AAccelerationPickUp::AAccelerationPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Collision Mesh"));
	StaticMesh->SetNotifyRigidBodyCollision(true);
	SetRootComponent(StaticMesh);

	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle System"));
	ParticleSystemComponent->bAutoActivate = false;

	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AAccelerationPickUp::TriggerEnter);
	StaticMesh->OnComponentEndOverlap.AddDynamic(this, &AAccelerationPickUp::TriggerExit);
}

// Called when the game starts or when spawned
void AAccelerationPickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAccelerationPickUp::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


void AAccelerationPickUp::TriggerEnter(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (Cast<AShipPawn>(OtherActor)) {
		
		ParticleSystemComponent->SetRelativeLocation(SweepResult.Location);
		ParticleSystemComponent->Activate();
	}
}

void AAccelerationPickUp::TriggerExit(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

	//TODO check if better way to check if Pawn Hit
	if (Cast<AShipPawn>(OtherActor)) {
		APlayerController* CP = UGameplayStatics::GetPlayerController(GetWorld(), 0);

		AShipPawn* Ship = Cast<AShipPawn>(CP->GetPawn());
		Ship->Accelerate();

	}

}
