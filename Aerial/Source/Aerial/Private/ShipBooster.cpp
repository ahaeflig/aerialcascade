// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "ShipBooster.h"
#include "ShipPawn.h"


UShipBooster::UShipBooster()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UShipBooster::MoveForward()
{

	auto ForceApplied = FVector(1.0f, 0.f, 0.f) * MaxSpeedForceForward;
	auto ForceLocation = GetComponentLocation();

	auto TankRoot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());
	TankRoot->AddForceAtLocation(ForceApplied, ForceLocation);
}

void UShipBooster::MoveFromTouch(float X, float Y) 
{
	FVector ImpulseDir = FVector(0.f, X, Y);
	ImpulseDir.Normalize();
	FVector MoveDir = ImpulseDir * MaxSpeedForceSideWays;

	auto ForceLocation = GetComponentLocation();
	auto ShipRoot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());


	FVector Velocity = ShipRoot->GetPhysicsLinearVelocityAtPoint(ForceLocation);
	Velocity.Normalize();
	/*
	LogTemp:Warning: Diff: X=0.555 Y=0.832 Z=-0.001
	LogTemp:Warning: Veloc: X=0.558 Y=0.830 Z=-0.001
	LogTemp:Warning: ImpulseDir: X=0.000 Y=-0.000 Z=0.000
	*/

		UE_LOG(LogTemp, Warning, TEXT("Veloc: %s"), *Velocity.ToString())
		UE_LOG(LogTemp, Warning, TEXT("ImpulseDir: %s"), *ImpulseDir.ToString())

		//TODO
		//Check velocity vs move dir, for y and z axis,
		//if different sign in an axis, add force to counter act current velocity in different axis?
		FVector diff = Velocity - ImpulseDir;
		diff.X = 0.f;
		UE_LOG(LogTemp, Warning, TEXT("Diff: %s"), *diff.ToString())

	//if ()

	//ShipRoot->AddForceAtLocation( , ForceLocation);
	ShipRoot->AddForceAtLocation(MoveDir - diff * MaxSpeedForceSideWays , ForceLocation);

	auto Ship = Cast<AShipPawn>(GetOwner());
	//Ship->GetMovementComponent()->Velocity += MoveDir;

	//Ship->GetMovementComponent()->
}

void UShipBooster::DodgeFromTouch(float X, float Y)
{

	auto Ship = Cast<AShipPawn>(GetOwner());
	if (Ship->MakeShipDodge()) {

		auto ShipRoot = Cast<UPrimitiveComponent>(Ship->GetRootComponent());

		FVector ImpulseDir = FVector(0.f, X, Y);
		ImpulseDir.Normalize();

		FVector Impulse = ImpulseDir * DashImpulseForce;

		//TODO Play particle effect in direction of impulse
		ShipRoot->AddImpulse(Impulse);
	}
}

void UShipBooster::Accelerate() {
	//TODO Play fancy boosting animation

	MaxSpeedForceForward = MaxSpeedForceForward * 1.2;
}