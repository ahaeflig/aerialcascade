// Fill out your copyright notice in the Description page of Project Settings.

#include "Aerial.h"
#include "BasicPickUp.h"
#include "ShipPawn.h"

// Sets default values
ABasicPickUp::ABasicPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Collision Mesh"));
	StaticMesh->SetNotifyRigidBodyCollision(true);
	SetRootComponent(StaticMesh);
	
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ABasicPickUp::TriggerEnter);
	StaticMesh->OnComponentEndOverlap.AddDynamic(this, &ABasicPickUp::TriggerExit);

}

// Called when the game starts or when spawned
void ABasicPickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasicPickUp::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABasicPickUp::TriggerEnter(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) 
{

	//TODO check if better way to check if Pawn Hit
	if (Cast<AShipPawn>(OtherActor)) {
		APlayerController* CP = UGameplayStatics::GetPlayerController(GetWorld(), 0);

		AShipPawn* Ship = Cast<AShipPawn>(CP->GetPawn());
		Ship->Heal(HealthAmount);

		Destroy();
	}
}

void ABasicPickUp::TriggerExit(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}