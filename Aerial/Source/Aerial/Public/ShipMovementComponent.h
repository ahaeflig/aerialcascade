// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/MovementComponent.h"
#include "ShipMovementComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class AERIAL_API UShipMovementComponent : public UMovementComponent
{
	GENERATED_BODY()
public:
	UShipMovementComponent();
	
	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	
	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialise(class UShipBooster* BoosterToSet);

	void BeginPlay();

	void MoveForward();

	void IntendMoveUp(float AxisValue);
	void IntendMoveRight(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void GetTouchInformation(FVector2D firstFinger, bool isPressedFirstFinger, FVector2D secondFinger, bool isPressedSecondFinger);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void ManageMovement();

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void TouchInitFirstFinger(FVector2D Vec);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void TouchInitSecondFinger(FVector2D Vec);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void GetPhoneTilt(FVector Vec);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void ManageShipTilt(float MoveX, float MoveY);


private:
	UShipBooster* Booster = nullptr;

	FVector2D FirstFingerLast = FVector2D(0.f, 0.f);
	FVector2D SecondFingerLast = FVector2D(0.f, 0.f);

	FVector2D FirstFingerCurrent = FVector2D(0.f,0.f);
	bool isFirstFingerPressed = false;
	FVector2D SecondFingerCurrent = FVector2D(0.f,0.f);
	bool isSecondFingerPressed = false;

	UPROPERTY(EditDefaultsOnly, Category = "Input")
	float SwipeMoveThresholdPercent = 0.05f;

	UPROPERTY(EditDefaultsOnly, Category = "Input")
	float TiltAngleFactor = 20.f;

	FRotator TargetRotation;
	FVector PhoneTilt;

};
