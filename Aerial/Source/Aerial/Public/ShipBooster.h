// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "ShipBooster.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class AERIAL_API UShipBooster : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	void MoveForward();

	void MoveFromTouch(float MoveX, float MoveY);

	void DodgeFromTouch(float MoveX, float MoveY);

	void Accelerate();

private:
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxSpeedForceForward = 1000000.0;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxSpeedForceSideWays = 2000000.0;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float DashImpulseForce = 500000.0f;

	UShipBooster();
	
};
