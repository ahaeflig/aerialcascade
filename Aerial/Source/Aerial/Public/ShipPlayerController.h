// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "ShipPlayerController.generated.h"

/**
 * Responsible for player control
 */
UCLASS()
class AERIAL_API AShipPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;
	
	UFUNCTION()
	void OnPossessedShipDeath();

	void LeaveGame();

	// Reference UMG Asset in the Editor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> WLoseScreen;

	// Variable to hold the widget After Creating it.
	UUserWidget* WMyLoseScreen;
	
};
