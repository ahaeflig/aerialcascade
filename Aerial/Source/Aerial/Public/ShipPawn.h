// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "ShipPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShipDelegate);

// Enum for Ship State
UENUM()
enum class EShipState : uint8
{
	Normal,
	Invincible,
	Dodging
};


UCLASS()
class AERIAL_API AShipPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AShipPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialise(class UShipBooster* BoosterToSet);

	UFUNCTION(BlueprintCallable, Category = "Movement")
	bool IsDodging();

	virtual float TakeDamage
	(
		float DamageAmount,
		struct FDamageEvent const & DamageEvent,
		class AController * EventInstigator,
		AActor * DamageCauser
	) override;

	void Heal(int32 amount);

	void Charge(int32 amount);

	void Accelerate();


	//Also speed upgrade counter
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 ScoreMultiplier = 1;

	//Also speed upgrade counter
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float Score = 0.f;

	// Return current health as a percentage of starting health between 0 and 1
	UFUNCTION(BlueprintPure, Category = "Setup")
	float GetHealthPercent() const;

	UFUNCTION(BlueprintPure, Category = "Setup")
	float GetEnergyPercent() const;

	UFUNCTION(BlueprintPure, Category = "Setup")
	int32 GetScore() const;

	UFUNCTION(BlueprintPure, Category = "Setup")
	int32 GetScoreMultiplier() const;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 StartHealth = 100;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	int32 CurrentHealth;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 StartEnergy = 100;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	int32 CurrentEnergy;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 DodgeEnergyCost = 10;

	class UShipBooster* Booster = nullptr;

	FShipDelegate OnShipDeath;
	
	EShipState ShipState = EShipState::Normal;

	UFUNCTION(BlueprintPure, Category = "Setup")
	EShipState GetShipState();

	bool MakeShipDodge();

	UFUNCTION(BlueprintPure, Category = "Game Mechs")
	bool CanShipDodge() const;

	//Delay after being damaged before retaking damage
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float InvincibleDelay = 3.f;

	//Delay after dodging before retaking damage and being able to dodge
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float DodgeDelay = 2.f;

	void ResetInvicibilityAndDodge();
};
